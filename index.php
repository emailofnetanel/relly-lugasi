<?php 

    // Form Validation
    // Check if POST REQUEST not empty
    if (!empty($_POST) && isset($_POST['submit'])) {

        $fullName = $_POST['fullName'];
        $phoneNumber = $_POST['phoneNumber'];

        if (!empty($fullName) && !empty($phoneNumber)) {
            if ((strlen($phoneNumber) >= 10) && (strlen($phoneNumber) < 14)) {
                // Send email
                $to = 'emailofnetanel@gmail.com';
                $subject = 'הודעה חדשה מהאתר של רלי';
                $message = wordwrap('השם: ' . $fullName . ' ' . ' הטלפון: ' . $phoneNumber . ' ');
                $headers = array(
                    'From'  => 'rellylugassi@website.com',
                    'Reply-To'  => 'rellylugassi@website.com',
                    'X-Mailer'  => 'PHP/' . phpversion()
                );
                mail($to, $subject, $message, $headers);
                header('Location: http://www.rellylugasi.com/thankyou.php');
            } else {
                // Please insert valid Israeli phone number.
                $valid_phone = '<div class="valid-phone"><svg style="fill:black;font-weight: bold; width: 3rem; height: 3rem;" class="close">
                                <use xlink:href="img/sprite.svg#icon-cross"></use>
                                </svg>Please insert valid Israel phone number</div>';
            }
        } else {
            // Please insert your full name and phone number.
            $fill_all =  '<div class="fill-all"><svg style="fill:black;font-weight: bold; width: 3rem; height: 3rem;" class="close">
                         <use xlink:href="img/sprite.svg#icon-cross"></use>
                         </svg>Please insert your full name and phone number.</div>';
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WTFK4G4');</script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Makeup artist" />
    <meta property="og:image" content="img/astar-b-2.jpeg">
    <meta
      name="keywords"
      content="Makeup,Beauty,Design,Pretty,Israel,Artist,Cosmetics,Personal,Care,Aesthetic,Skin,Blush,Rouge,Lipstick,Lips,Art"
    />
    <meta name="author" content="Netanel Vaknin" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.jpg">
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat:200"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Staatliches"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Pacifico"
      rel="stylesheet"
    />

    <title>Relly lugasi - Makeup artist official website</title>
  </head>

  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTFK4G4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- TOP NAVBAR -->
    <nav class="nav">
      <ul class="nav__list">
        <li class="nav__list-item--1">
          <a href="tel:+97254-561-8391">
            <svg
              style="fill: white; font-weight: bold; width: 2rem; height: 2rem;"
            >
              <use xlink:href="img/sprite.svg#icon-phone"></use>
            </svg>
          </a>
        </li>
        <li class="nav__list-item--2">
          <a href="https://www.instagram.com/makeup_by_relly/">
            <svg
              style="fill:white; font-weight: bold; width: 2rem; height: 2rem;"
            >
              <use xlink:href="img/sprite.svg#icon-instagram"></use>
            </svg>
          </a>
        </li>
        <div class="ecolizer-box" onClick="togglePlay()">
          <div class="ecolizer-box__cancel"></div>
          <div class="ecolizer-box__item ecolizer-box__item--1"></div>
          <div class="ecolizer-box__item ecolizer-box__item--2"></div>
          <div class="ecolizer-box__item ecolizer-box__item--3"></div>
          <div class="ecolizer-box__item ecolizer-box__item--4"></div>
        </div>
      </ul>
    </nav>
    <!-- END TOP NAVBAR -->
    <main>
      <!-- MAIN MENU -->
      <ul class="main-menu">
        <span class="main-menu__quote"
          >" <span style="color:#f3ff19;">Be yourself.</span> everyone else is
          already taken. "</span
        >
        <span class="main-menu__brand">RELLY LUGASI</span>
        <span class="main-menu__job" style="color:#f3ff19;">Freelance makeup artist</span>
        <li
          class="main-menu__item main-menu__item--1"
          onclick="openPortfolio()"
        >
          Portfolio
        </li>
        <li class="main-menu__item main-menu__item--2">About me</li>
        <li class="main-menu__item main-menu__item--3">Contact Relly</li>
      </ul>
      <!-- PORTFOLIO POPUP -->
      <div class="portfolio-popup">
        <svg
          style="font-weight: bold; width: 3rem; height: 3rem;"
          class="close"
        >
          <use xlink:href="img/sprite.svg#icon-cross"></use>
        </svg>
        <div class="portfolio-popup__image">
          <!-- The images will be injected from JS images generator function -->
        </div>
        <a href="#" class="portfolio-popup__next">Next</a>
        <a href="#" class="portfolio-popup__previous">Previous</a>
        <span class="portfolio-popup__quote"
          >"I always find <span style="color:#f3ff19;">beauty</span> in things
          that are odd and imperfect, they are much more interesting."</span
        >
      </div>
      <!-- END PORTFOLIO POPUP -->
      <!-- END MAIN MENU -->
    </main>
    <!-- FOOTER -->
    <footer class="footer">
      <a href="#" class="footer__rights" style="color:#f3ff19;"
        >All rights reserved to Netanel Vaknin &copy;</a
      >
    </footer>
    <!-- END FOOTER -->
    <!-- ABOUT RELLY POPUP -->
    <div class="about-popup">
      <svg style="font-weight: bold; width: 3rem; height: 3rem;" class="close">
        <use xlink:href="img/sprite.svg#icon-cross"></use>
      </svg>
      <h1 class="about-popup__about">About me</h1>
      <p class="about-popup__about-text">
      Since I can remember, I have always felt a great fondness for makeup and grooming. My biggest dream was always to make people smile and feel more confident. With my makeup I found myself fulfilling this dream. I hope that with the help of the pictures I will present on this site, you will choose to be my clients and together we will create a perfect look that will upgrade your next event. Yours, Rely.
      </p>
      <span class="about-popup__quote"
        >" <span style="color: #f3ff19;">Style</span> is a way to say who you
        are without having to speak. "</span
      >
    </div>
    <!-- END ABOUT RELLY POPUP -->
    <!-- CONTACT POPUP -->
    <div class="contact-popup">
      <svg style="font-weight: bold; width: 3rem; height: 3rem;" class="close">
        <use xlink:href="img/sprite.svg#icon-cross"></use>
      </svg>
      <h1 class="contact-popup__heading">
        Please leave your details and we will contact you soon
      </h1>
      <form
        action="/"
        method="POST"
        class="contact-popup__form"
        autocomplete="off"
      >
        <label for="full-name" class="contact-popup__form--label"
          >Full Name</label
        >
        <input
          type="text"
          class="contact-popup__form contact-popup__form--input"
          name="fullName"
          id="full-name"
          
          placeholder="Please insert your full name"
        />
        <label for="phone-number" class="contact-popup__form--label"
          >Phone Number</label
        >
        <input
          type="text"
          class="contact-popup__form contact-popup__form--input"
          name="phoneNumber"
          id="phone-number"
          
          maxlength="14"
          placeholder="Please insert valid Israeli number"
        />
        <input
          type="submit"
          class="contact-popup__form contact-popup__form--input"
          name="submit"
          value="SEND"
        />
      </form>
    </div>
    <?php if (isset($fill_all)) { echo $fill_all; } ?>
    <?php if (isset($valid_phone)) { echo $valid_phone; } ?>
    <!-- END CONTACT POPUP -->
    <!-- AUDIO -->
    <audio controls loop id="audio">
      <source src="audio/ZHU - Still want you.mp3" type="audio/mp3" />
      <p>Your browser doesn't support HTML5 audio.</p>
    </audio>
    <!-- END AUDIO -->
    <script src="scripts/main.js"></script>
  </body>
</html>
