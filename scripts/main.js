window.onload = function() {
    var portfolioImgContainer = document.querySelector('.portfolio-popup__image');
    var nextBtn = document.querySelector('.portfolio-popup__next');
    var previousBtn = document.querySelector('.portfolio-popup__previous');
    var customerPhotos = [
        'img/astar-b-1.jpeg',
        'img/astar-b-2.jpeg',
        'img/astar-l-1.jpeg',
        'img/astar-l-2.jpeg',
        'img/bathen-1.jpeg',
        'img/bathen-2.jpeg',
        'img/eden-1.jpeg',
        'img/eden-2.jpeg',
        'img/gal-1.jpeg',
        'img/gal-2.jpeg',
        'img/lotan-1.jpeg',
        'img/lotan-2.jpeg'
    ];

    // Generate manual images for beginning (For performance purposes)
    for (var i = 0; i < customerPhotos.length; i++) {
        this["img" + i] = document.createElement('img');
        this["img" + i].setAttribute('alt', 'Customer photo');
        this["img" + i].setAttribute('class', `portfolio-popup__image--the-image`);
        this["img" + i].setAttribute('src', `${customerPhotos[i]}`);
        // Fire the images to the page
        portfolioImgContainer.appendChild(this["img" + i]);

    }

    // Listen to clicks on next and previous buttons
    var counter = 0;
    var zIndexCount = 0;
    nextBtn.addEventListener('click', function() {
        counter++;
        zIndexCount++;

        // Reset the counter because no more photos left
        if (counter >= customerPhotos.length) {
            counter = 0; 
        }

        // Increase the zIndex for specific image for new layer
        document.querySelectorAll('.portfolio-popup__image--the-image')[counter].style.zIndex = zIndexCount;
        console.log(counter);
    });

    previousBtn.addEventListener('click', function() {
        if (counter == 0) {
            counter = customerPhotos.length;
        }
        zIndexCount++;
        counter--;
        
        // Increase the zIndex for specific image for new layer
        document.querySelectorAll('.portfolio-popup__image--the-image')[counter].style.zIndex = zIndexCount;
    });
}

// Check mobile for phone icon replacement
var phoneIcon = document.querySelector('.nav__list-item--1 a');
console.log(phoneIcon);
if(window.innerWidth > 800 && window.innerHeight > 600) {
    phoneIcon.innerText = '054-561-8391';
}

var validPhone = document.querySelector('.valid-phone');
var fillAll = document.querySelector('.fill-all');

var portfolioBtn = document.querySelector('.main-menu__item--1');
var portfolioContainer = document.querySelector('.portfolio-popup');
function openPortfolio() {
    portfolioContainer.style.display = 'flex';
}

var aboutBtn = document.querySelector('.main-menu__item--2');
var aboutContainer = document.querySelector('.about-popup');
aboutBtn.addEventListener('click', function() {
    aboutContainer.style.display = 'flex';
});

// Handle closing popups
var closeBtn = document.querySelectorAll('.close');
for (var i = 0; i < closeBtn.length; i++) {
    closeBtn[i].addEventListener('click', function() {
        portfolioContainer.style.display = 'none';
        aboutContainer.style.display = 'none';
        contactPopup.style.display = 'none';
        
        if (fillAll) {
            fillAll.style.display = 'none';
        }

        if (validPhone) {
            validPhone.style.display = 'none';
        }

    });
}

// Opening and closing contact popup
var contactPopup = document.querySelector('.contact-popup');
var contactRelly = document.querySelector('.main-menu__item--3');
contactRelly.addEventListener('click', function() {
    contactPopup.style.display = 'flex';
});

// Enable and disable audio
var audio = document.querySelector('#audio');
var ecolizer = document.querySelector('.ecolizer');
var cancel = document.querySelector('.ecolizer-box__cancel');
var isPlaying = false;

function togglePlay() {
    if (audio.paused) {
        cancel.style.display = 'none';
    } else {
        cancel.style.display = 'block';
    }
    return audio.paused ? audio.play() : audio.pause();
};