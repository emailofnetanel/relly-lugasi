<?php 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Relly lugasi - Thank you page</title>
    <link rel="stylesheet" href="css/style.css" />
</head>
<body>
   <div class="thankyou-box">
       <h1 class="thankyou-box__heading">Thank you</h1>
       <p class="thankyou-box__soon">I will contact you soon . . .</p>
       <div class="thankyou-box__timer"></div>
   </div>
   <script src="scripts/thankyou.js"></script>
</body>
</html>